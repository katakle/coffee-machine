package coffemaker.finances;

import java.math.BigDecimal;
import java.util.ArrayList;

import static java.util.Arrays.asList;


public class Wallet implements CoinsHolder {
    private static Wallet INSTANCE;
    private BigDecimal cash;
    private ArrayList<Coins> ownedCoins = new ArrayList<>(asList(Coins.FIVE, Coins.FIFTY));

    public static Wallet getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Wallet();
        }
        return INSTANCE;
    }

    private Wallet() {
        restockCoins();
        this.cash = calcCash();
    }

    private void restockCoins() {
        ArrayList<Coins> coins = new ArrayList<>();
        for (int i = 0; i < 1; i++) {
            coins.add(Coins.ONE);
            coins.add(Coins.TWO);
            coins.add(Coins.FIVE);
            coins.add(Coins.FIFTY);
            coins.add(Coins.TWENTY);
            coins.add(Coins.TEN);
        }
        setOwnedCoins(coins);
    }

    public void showCoins() {
        System.out.println("Monety: ");
        for (Coins coin : ownedCoins) {
            System.out.print(coin.getCoinName() + "; ");
        }
        System.out.print("\n");
    }

    private BigDecimal getCash() {
        return cash;
    }

    private void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public ArrayList<Coins> getOwnedCoins() {
        return ownedCoins;
    }

    private void setOwnedCoins(ArrayList<Coins> ownedCoins) {
        this.ownedCoins = ownedCoins;
    }

    @Override
    public BigDecimal calcCash() {
        BigDecimal calculatedCash = BigDecimal.valueOf(0);
        if (!ownedCoins.isEmpty()) {
            for (Coins coin : ownedCoins) {
                calculatedCash = calculatedCash.add(coin.getCoinValue());
            }
        }
        return calculatedCash;
    }

    @Override
    public void insertCoin(Coins coin) {
        ownedCoins.add(coin);
        setCash(getCash().add(coin.getCoinValue()));

    }

    @Override
    public void removeCoin(Coins coin) {
        ownedCoins.remove(coin);
        setCash(getCash().subtract(coin.getCoinValue()));
    }

    @Override
    public boolean isNotEmpty() {
        return getCash().compareTo(BigDecimal.ZERO) > 0;
    }

    @Override
    public boolean containsCoin(Coins coin) {
        for (Coins ownedCoin : ownedCoins) {
            if (coin.equals(ownedCoin)) {
                return true;
            }
        }
        return false;
    }
}
