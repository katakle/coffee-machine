package coffemaker.finances;

import coffemaker.Main;
import coffemaker.userinput.UserInput;
import coffemaker.drinks.Drink;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.math.BigDecimal.ZERO;

public class FinancialOperations {
    private Wallet wallet = Wallet.getInstance();
    private CoffeeMaker coffeeMaker = CoffeeMaker.getInstance();
    private UserInput userInput = new UserInput();

    public boolean isCashAvailable() {
        if (wallet.isNotEmpty() && coffeeMaker.isNotEmpty()) {
            return true;
        } else if (!wallet.isNotEmpty()) {
            System.out.println("Skończyły Ci się pieniądze!");
            return false;
        } else {
            return false;
        }
    }

    public void prepareCoffee(Drink drink, Scanner scanner) throws InterruptedException {
        drink.showInfo();
        if (pay(drink, scanner)) {
            drink.prepareDrink();
        } else {
            System.out.println("Nie masz wystarczającej ilości monet by zapłacić za kawę!");
            Main.run = false;
        }
    }

    private boolean pay(Drink coffee, Scanner scanner) {
        BigDecimal putCoins = ZERO;
        boolean paid;
        if (!hasEnoughCash(coffee, Wallet.getInstance())) {
            paid = false;
        } else {
            while (true) {
                if (coffee.getPrice().equals(putCoins)) {
                    paid = true;
                    break;
                } else if (coffee.getPrice().compareTo(putCoins) > 0) {
                    System.out.println("Do zapłaty: " + (coffee.getPrice().subtract(putCoins)));
                    BigDecimal money = insertCoinFromUser(scanner);
                    putCoins = putCoins.add(money);
                } else if (coffee.getPrice().compareTo(putCoins) < 0) {
                    BigDecimal change = putCoins.subtract(coffee.getPrice());
                    System.out.println("Reszta: " + (change));
                    giveChange(change);
                    putCoins = putCoins.subtract(change);
                }
            }
        }
        return paid;
    }

    private BigDecimal insertCoinFromUser(Scanner scanner) {
        Coins coin = userInput.getCoinFromUser(scanner);
        if (wallet.containsCoin(coin)) {
            wallet.removeCoin(coin);
            coffeeMaker.insertCoin(coin);
            return coin.getCoinValue();
        } else {
            System.out.println("Nie posiadasz takiej monety!");
            return ZERO;
        }
    }

    private void giveChange(BigDecimal change) {
        for (Coins coin : cashIntoCoins(change, coffeeMaker.getCoinsInMachine())) {
            wallet.insertCoin(coin);
            coffeeMaker.removeCoin(coin);
        }
        System.out.println("Wydano resztę: " + change.toString() + " zł");
    }

    private List<Coins> cashIntoCoins(BigDecimal cash, List<Coins> coinsAvailable) {
        BigDecimal remainedChange = cash;
        List<Coins> coins = new ArrayList<>();
        coinsAvailable.sort((o1, o2) -> Integer.compare(o2.getCoinValue().compareTo(o1.getCoinValue()), 0));
        for (Coins coin : coinsAvailable) {
            if ((remainedChange.compareTo(ZERO) == 0)) {
                return coins;
            } else if (coin.getCoinValue().compareTo(remainedChange) <= 0) {
                remainedChange = remainedChange.subtract(coin.getCoinValue());
                coins.add(coin);
            }
        }
        return coins;
    }

    private boolean hasEnoughCash(Drink drink, Wallet wallet) {
        return drink.getPrice().compareTo(wallet.calcCash()) <= 0;
    }
}
