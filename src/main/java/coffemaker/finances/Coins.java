package coffemaker.finances;

import java.math.BigDecimal;

public enum Coins {
    FIVE(BigDecimal.valueOf(5.0), "5"),
    TWO(BigDecimal.valueOf(2.0), "2"),
    ONE(BigDecimal.valueOf(1.0), "1"),
    FIFTY(BigDecimal.valueOf(0.5), "50"),
    TWENTY(BigDecimal.valueOf(0.2), "20"),
    TEN(BigDecimal.valueOf(0.1), "10"),
    NO_COIN(BigDecimal.ZERO, "nieistniejąca moneta");

    private BigDecimal coinValue;
    private String coinName;

    Coins(BigDecimal coinValue, String coinName) {
        this.coinValue = coinValue;
        this.coinName = coinName;
    }

    public String getCoinName() {
        return coinName;
    }

    public BigDecimal getCoinValue() {
        return coinValue;
    }
}
