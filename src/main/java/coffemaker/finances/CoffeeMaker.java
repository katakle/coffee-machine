package coffemaker.finances;

import java.math.BigDecimal;
import java.util.ArrayList;

import static java.math.BigDecimal.ZERO;

public class CoffeeMaker implements CoinsHolder {
    private static CoffeeMaker INSTANCE;
    private BigDecimal cash;
    private ArrayList<Coins> coinsInMachine;

    static CoffeeMaker getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CoffeeMaker();
        }
        return INSTANCE;
    }

    private CoffeeMaker() {
        restockCoins();
        this.cash = calcCash();
    }

    @Override
    public BigDecimal calcCash() {
        BigDecimal calculatedCash = ZERO;
        if (!coinsInMachine.isEmpty()) {
            for (Coins coin : coinsInMachine) {
                calculatedCash = calculatedCash.add(coin.getCoinValue());
            }
        }
        return calculatedCash;
    }

    @Override
    public void insertCoin(Coins coin) {
        coinsInMachine.add(coin);
        setCash(getCash().add(coin.getCoinValue()));
    }

    @Override
    public void removeCoin(Coins coin) {
        coinsInMachine.remove(coin);
        setCash(getCash().subtract(coin.getCoinValue()));
    }

    @Override
    public boolean isNotEmpty() {
        return getCash().compareTo(ZERO) > 0;
    }

    @Override
    public boolean containsCoin(Coins coin) {
        for (Coins ownedCoin : coinsInMachine) {
            if (coin.equals(ownedCoin)) {
                return true;
            }
        }
        return false;
    }

    ArrayList<Coins> getCoinsInMachine() {
        return coinsInMachine;
    }

    private void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    private BigDecimal getCash() {
        return cash;
    }

    private void setCoinsInMachine(ArrayList<Coins> coinsInMachine) {
        this.coinsInMachine = coinsInMachine;
    }

    private void restockCoins() {
        ArrayList<Coins> coins = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            coins.add(Coins.ONE);
            coins.add(Coins.TWO);
            coins.add(Coins.FIVE);
            coins.add(Coins.FIFTY);
            coins.add(Coins.TWENTY);
            coins.add(Coins.TEN);
        }
        setCoinsInMachine(coins);
    }
}
