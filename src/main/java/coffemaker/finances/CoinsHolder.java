package coffemaker.finances;

import java.math.BigDecimal;

public interface CoinsHolder {
    BigDecimal calcCash ();
    void insertCoin (Coins coin);
    void removeCoin (Coins coin);
    boolean isNotEmpty();
    boolean containsCoin(Coins coin);
}
