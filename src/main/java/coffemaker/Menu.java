package coffemaker;

import coffemaker.drinks.*;

class Menu {
    private static Mocha mocha = new Mocha();
    private static Cappuccino cappuccino = new Cappuccino();
    private static Espresso espresso = new Espresso();
    private static Latte latte = new Latte();
    private static LatteMacchiatto latteM = new LatteMacchiatto();

    private static String coffees =
            "1. Mocha " + mocha.getPrice() + " zł\n" +
                    "2. Latte " + latte.getPrice() + " zł\n" +
                    "3. Latte macchiatto " + latteM.getPrice() + " zł\n" +
                    "4. Espresso " + espresso.getPrice() + " zł\n" +
                    "5. Cappuccino " + cappuccino.getPrice() + " zł\n";

    static void printMenu() {
        System.out.println(coffees);
    }

    static void printGreeting() {
        System.out.println("Witamy!");
    }
}
