package coffemaker;

import coffemaker.drinks.*;
import coffemaker.finances.FinancialOperations;
import coffemaker.finances.Wallet;
import coffemaker.userinput.UserInput;

import java.util.Scanner;

public class Main {
    private static FinancialOperations financialOperations = new FinancialOperations();
    public static boolean run = financialOperations.isCashAvailable();
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {
        UserInput userInput = new UserInput();

        while (run) {
            Menu.printGreeting();
            Menu.printMenu();
            Wallet.getInstance().showCoins();
            int chosenId = userInput.chooseDrink(scanner);
            switch (chosenId) {
                case 1:
                    Drink mocha = new Mocha();
                    financialOperations.prepareCoffee(mocha, scanner);
                    break;
                case 2:
                    Drink latte = new Latte();
                    financialOperations.prepareCoffee(latte, scanner);
                    break;
                case 3:
                    Drink latteM = new LatteMacchiatto();
                    financialOperations.prepareCoffee(latteM, scanner);
                    break;
                case 4:
                    Drink espresso = new Espresso();
                    financialOperations.prepareCoffee(espresso, scanner);
                    break;
                case 5:
                    Drink cappuccino = new Cappuccino();
                    financialOperations.prepareCoffee(cappuccino, scanner);
                    break;
                default:
                    break;
            }
        }
    }
}
