package coffemaker.userinput;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Validator {
    boolean validateInputId(String givenString) {
        Pattern patternNames = Pattern.compile("[1-5]");
        Matcher m = patternNames.matcher(givenString);
        return m.matches();
    }

    boolean validateCoins (String givenString){
        Pattern patternNames = Pattern.compile("[1,2,5][0]?");
        Matcher m = patternNames.matcher(givenString);
        return m.matches();
    }
}
