package coffemaker.userinput;

import coffemaker.finances.Coins;

import java.util.Scanner;

public class UserInput {
    private Validator validator = new Validator();

    public int chooseDrink(Scanner scanner) {
        String chosenId;
        do {
            System.out.println("Podaj numer wybranej kawy");
            chosenId = scanner.nextLine();
        }
        while (!validator.validateInputId(chosenId));
        return Integer.parseInt(chosenId);
    }

    public Coins getCoinFromUser(Scanner scanner) {
        String userInput;
        do {System.out.println("Włóż monety...");
            userInput = scanner.nextLine();
        }
        while (!validator.validateCoins(userInput));
        for (Coins coin : Coins.values()) {
            if (userInput.equals(coin.getCoinName())) {
                return coin;
            }
        }
        return Coins.NO_COIN;
    }

}




