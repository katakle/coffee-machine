package coffemaker.drinks;

import java.math.BigDecimal;

public class Espresso extends Drink {
    private final String name;

    public Espresso() {
        this.name = "Espresso";
        this.id = 4;
        this.price = BigDecimal.valueOf(2.5);
        this.prepTime = 10000;
    }

    @Override
    public String getName() {
        return name;
    }
}
