package coffemaker.drinks;

import java.math.BigDecimal;

public class Mocha extends Drink {
    private final String name;

    public Mocha() {
        this.name = "Mocha";
        this.id = 1;
        this.price = BigDecimal.valueOf(3.0);
        this.prepTime = 10000;
    }

    @Override
    public String getName() {
        return name;
    }
}
