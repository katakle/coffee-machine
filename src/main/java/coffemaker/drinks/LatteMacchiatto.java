package coffemaker.drinks;

import java.math.BigDecimal;

public class LatteMacchiatto extends Drink {
    private final String name;

    public LatteMacchiatto() {
        this.name = "Latte Macchiatto";
        this.id = 3;
        this.price = BigDecimal.valueOf(3.0);
        this.prepTime = 1000;
    }

    @Override
    public String getName() {
        return name;
    }
}
