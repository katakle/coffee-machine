package coffemaker.drinks;

import java.math.BigDecimal;

public abstract class Drink {
    private String name;
    BigDecimal price;
    int prepTime;
    int id;

    Drink() {
        this.name = "kawa";
        this.price = getPrice();
        this.prepTime = getPrepTime();
        this.id = getId();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public void prepareDrink() throws InterruptedException {
        System.out.println("Poczekaj, aż Twoja kawa będzie gotowa");
        Thread.sleep(prepTime);
        System.out.println("Kawa gotowa do odbioru!");
    }

    public void showInfo() {
        System.out.println("Wybrałeś: " + getName() + ". Do zapłaty: " + getPrice() + "zł.");
    }

    private int getId() {
        return id;
    }

    private int getPrepTime() {
        return prepTime;
    }
}
