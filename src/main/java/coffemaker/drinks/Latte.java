package coffemaker.drinks;

import java.math.BigDecimal;

public class Latte extends Drink {
    private final String name;

    public Latte() {
        this.name = "Latte";
        this.id = 2;
        this.price = BigDecimal.valueOf(3.0);
        this.prepTime = 10000;
    }

    @Override
    public String getName() {
        return name;
    }
}
