package coffemaker.drinks;

import java.math.BigDecimal;

public class Cappuccino extends Drink {
    private final String name;

    public Cappuccino() {
        this.name = "Cappuccino";
        this.id = 5;
        this.price = BigDecimal.valueOf(3.0);
        this.prepTime = 10000;
    }

    @Override
    public String getName() {
        return name;
    }
}
